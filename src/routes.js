import Home from "./containers/Home";

export const routesPath = {
  HOME: "/"
};

export const routes = [
  {
    path: routesPath.HOME,
    exact: true,
    component: Home,
    customHeader: false
  }
];
