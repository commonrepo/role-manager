export const makeid = () => {
  var text = "";
  var possible =
    "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

  for (var i = 0; i < 30; i++)
    text += possible.charAt(
      Math.floor(Math.random() * Math.random() * possible.length)
    );

  return text;
};

export function getDate(input = "") {
  const currentdate = input ? new Date(input) : new Date();
  const datetime =
    currentdate.getFullYear() +
    "-" +
    ("0" + (currentdate.getMonth() + 1)).slice(-2) +
    "-" +
    ("0" + currentdate.getDate()).slice(-2);
  return datetime;
}

export function getTime(input = "") {
  const currentdate = input ? new Date(input) : new Date();
  const time =
    ("0" + currentdate.getHours()).slice(-2) +
    ":" +
    ("0" + currentdate.getMinutes()).slice(-2) +
    ":" +
    ("0" + currentdate.getSeconds()).slice(-2);
  return time;
}

export function getSunday(dateString) {
  const days = [
    "Sunday",
    "Monday",
    "Tuesday",
    "Wednesday",
    "Thursday",
    "Friday",
    "Saturday"
  ];
  const d = new Date(dateString);
  return days[d.getDay()] !== "Sunday";
}

export function getDateTime(input = "") {
  const currentdate = input ? new Date(input) : new Date();
  const datetime =
    currentdate.getFullYear() +
    "-" +
    ("0" + (currentdate.getMonth() + 1)).slice(-2) +
    "-" +
    ("0" + currentdate.getDate()).slice(-2) +
    "T" +
    ("0" + currentdate.getHours()).slice(-2) +
    ":" +
    ("0" + currentdate.getMinutes()).slice(-2);
  return datetime;
}

export const getSelectedItem = (myArray, id) => {
  let arr = myArray.filter(v => v.value !== "" && v.value == id);
  return arr.length > 0 && arr[0].value !== "" ? arr[0].label : "NA";
};

export const removeArrayElement = (array, values) => {
  values.map(val => {
    var index = array.indexOf(val.toString());
    if (index > -1) {
      array.splice(index, 1);
    }
  });
  return array;
};

export const objectToString = obj => {
  let string = "";
  for (let i in obj) {
    string = string + i + "=" + obj[i] + "<br />";
  }
  return string;
};

export const isEmpty = data => {
  if (typeof data == "number" || typeof data == "boolean") {
    return false;
  }
  if (typeof data == "undefined" || data === null) {
    return true;
  }
  if (typeof data.length != "undefined") {
    return data.length == 0;
  }
  var count = 0;
  for (var i in data) {
    if (data.hasOwnProperty(i)) {
      count++;
    }
  }
  return count == 0;
};

export const hasProp = (obj, prop) => {
  for (var p in obj) {
    if (obj.hasOwnProperty(p)) {
      if (
        p === prop &&
        obj[p] !== "" &&
        obj[p] !== undefined &&
        obj[p] !== null
      ) {
        return obj;
      } else if (obj[p] instanceof Object && hasProp(obj[p], prop)) {
        return obj[p];
      }
    }
  }
  return null;
};

export const getDateWithMonthName = (input = "") => {
  const monthNames = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December"
  ];
  const currentdate = input ? new Date(input) : new Date();
  const time = [
    monthNames[currentdate.getMonth()].slice(0, 3) +
      " " +
      currentdate.getDate(),
    currentdate
      .getFullYear()
      .toString()
      .slice(-2)
  ].join(", ");
  return time;
};
