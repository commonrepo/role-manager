import axios from "axios";
import { getCookie } from "../helpers/cookies";

export function signInUserApi(credentials) {
  return axios({
    url: "/auth-service/login",
    data: credentials,
    method: "POST",
    headers: {
      "Content-Type": "Application/json",
    }
  })
    .then(res => res)
    .catch(err => err.response);
}

export function signOutUserApi() {
  return axios({
    url: "/auth-service/logout",
    method: "POST",
    headers: {
      "Content-Type": "Application/json",
      "X-AUTH-TOKEN": getCookie("_token")
    }
  })
    .then(res => res.data)
    .catch(err => err);
}

export function validateTokenApi(cb) {
  return axios({
    url: "/auth-service/validate-token",
    method: "GET",
    headers: {
      "Content-Type": "Application/json",
      "X-AUTH-TOKEN": getCookie("_token")
    }
  })
    .then(res => res.data.data == null && cb())
    .catch(err => cb());
}
