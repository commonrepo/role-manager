const express = require("express");
const path = require("path");
const bodyParser = require("body-parser");
import logger from "../utils/logger";
import { getDateTime } from "../src/helpers";
const cookieParser = require("cookie-parser");
const app = express();
app.use(cookieParser());
app.use(bodyParser.json({ limit: "50mb" }));
app.use("/dist", express.static("dist"));
app.use(express.static("public"));
app.get("*", function(req, res) {
  res.sendFile(path.join(__dirname, "../public/index.html"), function(err) {
    if (err) {
      logger.error(
        "error-2 for API At " + getDateTime() + " => " + req.originalUrl
      );
      res.status(500).send(err);
    }
  });
});

process.on("unhandledRejection", function(err) {
  logger.error("UnhandledRejection error" + err);
});
process.on("uncaughtException", function(error) {
  logger.error("uncaughtException" + error);
  logger.error("Error Stack", error.stack);
});
app.listen(3000, () => console.log(`Example app listening on port 3000!`));
console.log(`Worker ${process.pid} started`);
