module.exports = {
  env: process.env.NODE_ENV,
  secret: "allaboutjavascript",
  access: {
    admin: { username: "admin", password: "admin", role: "admin" },
    user: { username: "user", password: "user", role: "user" }
  }
};
